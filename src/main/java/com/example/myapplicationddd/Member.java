package com.example.myapplicationddd;

public class Member {

 private float temp;
 private float humidity;
 private float gasReading;

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getHumidity() {
        return humidity;
    }

    public void setHumidity(float humidity) {
        this.humidity = humidity;
    }

    public float getGasReading() {
        return gasReading;
    }

    public void setGasReading(float gasReading) {
        this.gasReading = gasReading;
    }
}
