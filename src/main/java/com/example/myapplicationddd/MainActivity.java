package com.example.myapplicationddd;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    DatabaseReference reff ;
    Member member;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Intent tempIntent = new Intent(getApplicationContext(),TemperatureActivity.class);

        Button temperatureBtn = (Button) findViewById(R.id.temperature_btn);
        temperatureBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             
                //pass info to temperature activity
                startActivity(tempIntent);
            }
        });

        Button humidityBtn = (Button) findViewById(R.id.humidity_btn);
        humidityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tempIntent = new Intent(getApplicationContext(),HumidityActivity.class);
                //pass info to temperature activity
                startActivity(tempIntent);
            }
        });


        Button luminosityBtn = (Button) findViewById(R.id.luminosity_btn);
        luminosityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tempIntent = new Intent(getApplicationContext(),LuminosityActivity.class);
                //pass info to temperature activity
                startActivity(tempIntent);
            }
        });

        Button gasBtn = (Button) findViewById(R.id.gas_btn);
        gasBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tempIntent = new Intent(getApplicationContext(),GasDetectingActivity.class);
                //pass info to temperature activity
                startActivity(tempIntent);
            }
        });

        reff= FirebaseDatabase.getInstance().getReference().child("Member");

    }
}
